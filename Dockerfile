FROM php:8.2-apache-buster

COPY index.php /var/www/html/index.php
EXPOSE 80
CMD ["apache2-foreground"]